from queue import PriorityQueue
from queue import Queue

# this class is required in order to make entries of pairs of
# distance (=priority) and node (uncomparable) comparable
# Python does not accept tuples (dist, node) when node is not comparable
class PriorityEntry:

    def __init__(self, priority, data):
        self.data = data
        self.priority = priority

    def __lt__(self, other):
        return self.priority < other.priority

White = "white"
Grey = "grey"
Black = "black"

class Node:
    def __init__(self,x,y):
      self.x = x
      self.y = y
      self.edges = []
      
    def add_edge(self,e):
      self.edges.append(e)
      
    def print(self):
      print("node @", self.x, self.y, ", outgoing edges: ", len(self.edges))

class Edge:
    def __init__(self,weight,dst):
      self.target = dst
      self.weight = weight
    
    def print(self):
      print("egde with weight = ",weight," to node ", node)

class Graph:

  def __init__(self):
    self.nodes = {}
    self.start = None
    self.end = None

  def add_node(self, n):
    if n not in self.nodes:
      self.nodes[n] = n

  def print(self):
    for v in self.nodes:
      v.print()

  def BFS(self,v):
    
    color = {v: Grey}
    dist = {v:0}
    predecessor = {v: None}

    queue = Queue();
    queue.put(v);

    while not queue.empty():
      u = queue.get()
      for e in u.edges:
        w = e.target
        if w not in color: # color is white
          color[w] = Grey
          predecessor[w] = u
          dist[w] = dist[u] + e.weight
          queue.put(w)
      color[u] = Black
    return (dist, predecessor)


  def DFSR(self, v, color, dist, predecessor):
    color[v] = Grey
    for e in v.edges:
      w = e.target
      if w not in color:
        predecessor[w] = v;
        dist[w] = dist[v] + e.weight
        self.DFSR(w,color,dist,predecessor);
    color[v] = Black

  def nextWhite(self,edges,color):
    for e in edges:
      if e.target not in color:
        return e
    return None;

  # iterative versions of recursive DFS (recursion overflows!)
  def DFSI(self, v, color, dist, predecessor):
    stack = []
    color[v] = Grey
    stack.append(v)
    while stack:
      e = self.nextWhite(v.edges,color);
      if e != None:
        w = e.target;
        color[w] = Grey
        dist[w] = dist[v] + e.weight
        predecessor[w] = v
        stack.append(w)
      else:
        color[v]= Black
        if stack:
          v = stack[len(stack)-1]
          if color[v] == Black:
            del stack[len(stack)-1]

  def DFS(self, s):
    color = {s: Grey}
    dist = {s: 0}
    predecessor = {s: None}
    self.DFSI(s,color,dist,predecessor)
    return (dist, predecessor)




  def Generic(self, s):
    rcount = 0
    dist = {s:0}
    predecessor = {s: None}
    
    relax = True
    while relax:
      relax = False
      for u in self.nodes:
        if u in dist:
          for e in u.edges:
            v = e.target;
            d = dist[u] + e.weight
            if v not in dist or d < dist[v]:
              dist[v] = d
              predecessor[v] = u
              relax = True
            rcount = rcount + 1
    print("rcount = ", rcount)
    return (dist, predecessor)


  '''
    Please note that there are many ways of writing this algorithm. This master solution uses lazy deletion,
    which is a concept introduced in Exercise 8, "Code reading BFS". Of course, the other approach is also possible.
    This solution is DIFFERENT from the pseudocode in the slides and on Wikipedia. For example, there is no
    DecreaseKey operation, just a workaround (possible due to lazy deletion) on line 179. 
    Also, the distances are not initialized to infinity as shown in the slides.
  '''


  # Dijkstra is called on the start node s
  def Dijkstra(self, s):
    # this variable is used to estimate the runtime.
    rcount = 0

    # dict that saves the distance of each node, initialize start node distance to 0
    dist = {s:0}

    # for each node, save its predecessor
    predecessor = {s: None}

    # initialize a priority queue with the start node at priority value 0
    R = PriorityQueue()
    R.put(PriorityEntry(0,s))

    # as long as there are nodes in the PriorityQueue R:
    while not R.empty():
      
      # dequeue the element with lowest priority value
      # p contains both the priority value and the node name
      # u contains only the node name
      p = R.get()
      u = p.data

      # check if the priority value in the PriorityQueue is the same as the newest known distance
      if p.priority == dist[u]: # lazy deletion
        # i iterate through all successors of the current node
        for e in u.edges:
          # compute total distance to successor
          v = e.target
          w = dist[u] + e.weight
          # if the successor has not been computed yet, or the distance is improvable through current node u
          if v not in dist or w < dist[v]:
            # update the distance for the successor and add it to the PriorityQueue
            dist[v] = w
            predecessor[v] = u
            R.put(PriorityEntry(w,v))
          
          # update runtime counter
          rcount = rcount + 1
    
    print("rcount = ", rcount)
    return (dist, predecessor)
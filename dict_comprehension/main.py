accounts = {
    'Food' : { 'Amount' : 1242, 'Kind': 'Credit' },
    'Insurances' : { 'Amount' : 5547, 'Kind': 'Credit' },
    'Fun-Time' : { 'Amount' : 3978, 'Kind': 'Credit'},
    'Salary'   : { 'Amount': 14785, 'Kind': 'Debit' },
    'Jewelry' : { 'Amount': 14785, 'Kind': 'Debit' }
}

credit_accounts = { key : accountBalance['Amount'] for key, accountBalance in accounts.items() 
if accountBalance['Kind'] == 'Credit'}  # create your dict here

print(credit_accounts)

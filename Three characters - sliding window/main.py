from termcolor import cprint

def main():
  text = input()

  # hint: use this dictionary
  map = {'a':0, 'b':0, 'c':0}
  
  bestl = -1
  bestr = len(text)
  l=0
  r=-1
  
  '''
  num == 0: a, b or c not in interval
  num == 1: 1 of the letters is present at least once
  num == 2: 2 of the letters are present at least once
  num == 3: a, b or c are all present at least once
  '''
  num=0
  

  '''
  Sliding Window algorithm.
  
  This algorithm improves the runtime from O(n^3) in the naive implementation,
  where we check every possible interval, to O(n). Informal reasoning: both
  l and r can move max. len(text)~n times, so we have a maximum of 2n movements
  of the window.
  '''

  #terminate when the right edge of the window goes out of range
  while r < len(text):

    #Current window is better than best window.
    if num == 3 and r - l < bestr - bestl:
      #update bestr, bestl
      bestr, bestl = r, l

    
    #a,b,c all in window - shrink window from the left
    if num >= 3:

      #get the element at the left edge
      nextl = text[l]
      
      #the left edge element is not a, b or c
      if nextl not in map:
        l += 1
        
      #the left edge element is a, b or c
      elif nextl in map:
        l += 1
        
        #as a, b or c has dropped out of the window, update map accordingly
        map[nextl] -= 1
        
        #if the last instance of a, b or c has dropped out of the window, update num
        if map[nextl] == 0:
          num -= 1
          
    #a, b and/or c not in window - extend to the right
    else:
      #extend window
      r += 1
      
      #check that the right edge of the window is still in range
      if r < len(text):
        
        #get the right edge element
        nextr = text[r]
        
        #if the right edge is a, b or c, update map and num accordingly
        if nextr in map:
          if map[nextr] == 0:
            num += 1
          map[nextr] += 1
      
      
  if bestl == -1:
    print(text,"does not contain a,b AND c.")
  else:
    print(text,"contains a,b,c between",bestl,"and",bestr,":",text[bestl : bestr+1])
    
def buildMap (A, l, r):
  m = {'a':0, 'b':0, 'c':0}
  for e in A[l:r]:
    if e == 'a':
      m['a'] += 1
    elif e == 'b':
      m['b'] += 1
    elif e == 'c':
      m['c'] += 1
      
  return A
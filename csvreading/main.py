import csv

balance = 0

with open('data.csv', 'r') as file:
  accounts = csv.DictReader(file)
  for e in accounts:
    if not e['Debit'] == '-':
      balance += int(e['Debit'])
    if not e['Credit'] == '-':
      balance -= int(e['Credit'])
      
# Open file here and extract the requestend information.
# use the `with` block to automatically close the file afterwards.
print ('Balance:', balance)
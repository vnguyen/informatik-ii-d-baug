from SearchNode import *
from termcolor import cprint

class SearchTree:
    root = None   # Wurzelknoten

    def __init__(self):
        self.root = None

    '''
    Grundidee der Augmentierung: Wenn alle Nodes ihre Höhe speichern, beobachten wir,
    dass man nicht für alle Nodes die Höhen neu berechnen muss. Man muss nur die Höhen
    der Nodes "entlang dem Pfad" zum neuen Blatt aktualisieren. Das spart Laufzeit.
    Für n~Anzahl Nodes verbessern wir von O(n) zu O(h(n)).

    ------------------------------------------------------------------------------------

    PRE: updateHeights() wird nur auf Blätter aufgerufen (k ist ein Blatt).
    Der Grund: Neue Nodes werden in der Funktion add() immer als Blatt hinzugefügt.

    POST: Alle Nodes speichern die korrekte Höhe
    '''
    def updateHeights (self,n, k):
      '''
      Base case - Node gefunden und n ist ein Blatt: nichts machen, da man am Blatt ist, und n
      wurde wegen dem Konstruktor in SearchNode.py zu 1 initialisiert
      '''
    
      '''
      Recursive case - Node noch nicht gefunden, noch nicht am Blatt angekommen.
      '''
      if k < n.key:
        '''
        Neuer Node wurde links von n hinzugefügt. Wir müssen nur links überprüfen,
        um zu wissen ob die Höhe von n aktualisiert werden muss
        '''
        self.updateHeights(n.left, k)
        n.height = max(n.left.height + 1, n.height)
      if k > n.key:
        '''
        Neuer Node wurde rechts von n hinzugefügt. Wir müssen nur rechts überprüfen,
        um zu wissen ob die Höhe von n aktualisiert werden muss
        '''
        self.updateHeights(n.right, k)
        n.height = max(n.right.height + 1, n.height)

      

    def add(self, k):
        if self.root == None:
            self.root = SearchNode(k)
            return self.root
        t = self.root;
        while (True):
            if k == t.key:
                return None
            if k < t.key:
                if t.left == None:
                    newNode = SearchNode(k)
                    t.left = newNode
                    self.updateHeights(self.root,k)
                    return
                else:
                    t = t.left
            else:  # k > t.key
                if t.right == None:
                    newNode = SearchNode(k)
                    t.right = newNode
                    self.updateHeights(self.root,k)
                    return;
                else:
                    t = t.right

    def find(self, k):
      if self.root == None:
        return None;
      n = self.root;
      while n != None and n.key != k:
        if k < n.key:
          n = n.left
        else:
          n = n.right;
      return n


    def prettyprint(self, t, level = 0, prefix=[], location='root'):
      """This is a bit black magic, but luckily, you don't have to understand it.
      Just use it and enjoy a reasonably rendered tree in the console."""
      if t is None:
        print('(empty tree)')
        return
      if t.left is not None:
        prefix.append('  \u2503 ' if location == 'right' else '    ')
        self.prettyprint(t.left, level + 1, prefix, 'left')
        del prefix[-1]
      cprint(''.join(prefix) + ('  \u2501\u2501' if location == 'root' else \
      '  \u250f\u2501' if location == 'left' else '  \u2517\u2501'), \
      attrs=['dark'], end=' ')
      print(t.key)  
      if t.right is not None:
        prefix.append('  \u2503 ' if location == 'left' else '    ')
        self.prettyprint(t.right, level + 1, prefix, 'right')
        del prefix[-1]
from SearchTree import * 

def RepresentsInt(s):
  try: 
    int(s)
    return True
  except ValueError:
    return False

def main():
  tree = SearchTree()
    
  # Input
  while True:
    eingabe = input().split() #    inp = list(map(int,input().split()))
    commands = len(eingabe)
    c = 0
    while c < commands: 
      if eingabe[c] == "add":
        c += 1
        while c < commands and RepresentsInt(eingabe[c]):
          num = int(eingabe[c])
          tree.add(num)
          c += 1
      elif eingabe[c] == "height":
        c += 1;
        while c < commands and RepresentsInt(eingabe[c]):
          num = int(eingabe[c])
          n = tree.find(num)
          print("height (",num,") =",n.height if n != None else 0)
          c += 1
      elif eingabe[c] == "end":
        return
      elif eingabe[c] == "print":
        c += 1;
        print("[", end='')
        tree.prettyprint(tree.root)    # output of the tree in pre-order: node then left then right
        print(" ]")
      else:
        print("command?")
        eingabe[c] = "end"
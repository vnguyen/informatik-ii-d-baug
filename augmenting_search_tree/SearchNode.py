class SearchNode:
  def __init__(self, k, l=None, r=None):
    self.key = k
    self.left, self.right = l, r
    self.height = 1
    '''
    Jeder Node speichert seine eigene Höhe. Da man i.d.R. neue Nodes als Blatt
    initialisiert, setzen wir die default height zu 1.
    '''